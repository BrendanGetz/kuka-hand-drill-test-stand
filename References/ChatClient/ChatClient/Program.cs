﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Threading;
using CapToolBox;

namespace ChatClient
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Application.ApplicationExit += new EventHandler(OnApplicationExit);
			form1 = new Form1();
			Application.Run(form1);
		}

		static Form1 form1;

		private static void OnApplicationExit(object sender, EventArgs e)
		{
			form1.onExit();
		}
	}
}
