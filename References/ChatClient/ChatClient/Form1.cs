﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapToolBox;

namespace ChatClient
{
	public partial class Form1 : Form
	{

		BoostSoc bossSoc = new BoostSoc(false, 30);
		//BoostSoc KMbosSoc;

		bool programRunning = true;

		public Form1()
		{
			InitializeComponent();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			//bsConnections();
		}

		public void onExit()
		{
			programRunning = false;
			bossSoc.killBoostSoc();
		}



		private void readInMessages(BoostSoc connectedSoc)
		{
			bool conDroppedFlag = false;
			while (programRunning)
			{
				Thread.Sleep(25);
				string inMessage = connectedSoc.getLatestMessage();
				if (inMessage != "")
				{
          string recieved = "";//receiveTB.Text;
					
						recieved = inMessage + "\r\n" + recieved;		
					this.Invoke(new Action(() =>
					{
						receiveTB.Text = recieved;
					}));
				}
				if (!connectedSoc.getConnected() && !conDroppedFlag)
				{
					conDroppedFlag = true;
				}
				else if (connectedSoc.getConnected() && conDroppedFlag)
				{
					conDroppedFlag = false;
				}
			}
		}

		private void clientBtn_Click(object sender, EventArgs e)
		{
			bossSoc.clientConnect(2000, "127.0.0.1", false, true);
			Thread threadReadIn = new Thread(() => readInMessages(bossSoc));
			threadReadIn.Start();
		}

		private void serverBtn_Click(object sender, EventArgs e)
		{
			bossSoc.serverConnect(2000, false, true);
			Thread threadReadIn = new Thread(() => readInMessages(bossSoc));
			threadReadIn.Start();
		}

		private void timer1_Tick(object sender, EventArgs e)
		{
			if (bossSoc.getConnected())
			{
				connectLbl.Text = "Connected";
				connectLbl.BackColor = Color.Green;
			}
			else
			{
				connectLbl.Text = "Not Connected";
				connectLbl.BackColor = Color.Khaki;
			}
		}

		private void textBox1_KeyPressDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter) //Enter
			{
				if (bossSoc.getConnected())
				{
					bossSoc.sendMessage(sendTB.Text);
					sendTB.Text = "";
				}
				else
				{
					MessageBox.Show("No Connection Made");
				}
			}
		}

	}
}


