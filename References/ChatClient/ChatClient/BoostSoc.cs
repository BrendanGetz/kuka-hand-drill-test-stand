﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CapToolBox
{
	//Structure is heavily based off of BoostSoc
	//Allows server or client functionality and supports background management of connection dropping and message handling
	//Syntax is largely from :
	//https://docs.microsoft.com/en-us/dotnet/api/system.net.sockets.tcplistener?redirectedfrom=MSDN&view=netframework-4.8
	//https://docs.microsoft.com/en-us/dotnet/api/system.net.sockets.tcpclient?redirectedfrom=MSDN&view=netframework-4.8

	/// <summary>
	/// Boost sock port for C#
	/// </summary>
	public class BoostSoc
	{
		TcpListener listener = null;
		TcpClient connectedSocket;
		NetworkStream socStream = null;

		Thread connectionThread;
		Thread serialThread;
		bool socketManagementKillFlag = false;
		bool connectionKillFlag = false;

		String delimChar = ";";

		bool connected = false;
		bool listening = false;

		Stopwatch mainClock = new Stopwatch();
		long heartbeatPeriod = 50; //mS

		List<string> inMsgs = new List<string>();
		List<string> outMsgs = new List<string>();

		string ip;
		int port;
		bool debugPrints;
		bool autoReconnect;

		public BoostSoc(bool _debugPrint = false, int _period_mS = 50)
		{
			heartbeatPeriod = _period_mS;
			debugPrints = _debugPrint;

			mainClock.Restart();

			serialThread = new Thread(SerialManagement_DoWork);
			serialThread.Start();
		}

		//~BoostSoc()
		//{
		//  socketManagementKillFlag = true;
		//  connectionKillFlag = true;
		//  if (serialThread != null) { serialThread.Join(); }
		//  if (connectionThread != null) { connectionThread.Join(); }

		//  cleanupSoc();
		//}

		public void killBoostSoc()
		{
			socketManagementKillFlag = true;
			connectionKillFlag = true;
			if (serialThread != null) { serialThread.Join(); }
			if (connectionThread != null) { connectionThread.Join(); }

			cleanupSoc();
		}

		public bool getConnected() { return connected; }
		public bool getListening() { return listening; }

		/// <summary>
		/// Returns a list of messages from the incoming read message buffer
		/// </summary>
		/// <param name="count">-1 will read all messages from buffer, numbers greater than 0 will read the respective nuber of messages in order that they were received</param> 
		/// <param name="flush">if true, will delete all unread (after count pulled) messages from inBuffer</param> 
		/// <returns> list of all messages recieved since last flush</returns>
		public List<String> getMessages(int count = -1, bool flush = false)
		{
			List<String> tempMsgs = new List<String>();

			if (!connected || (count < 1 && count != -1)) { return tempMsgs; }

			lock (inMsgs)
			{
				if (count == -1) { count = inMsgs.Count(); }

				if (inMsgs.Count() > 0)
				{
					for (int i = 0; i < count && i < inMsgs.Count(); i++)
					{
						tempMsgs.Add((String)inMsgs[i].Clone());
					}

					if (flush) { inMsgs.Clear(); }
					else
					{
						inMsgs.RemoveRange(0, Math.Min(count - 1, inMsgs.Count() - 1));
					}
				}
			}
			return tempMsgs;
		}

		/// <summary>
		/// Gets next message FIFO from incomming que, will remove message from que on return
		/// </summary>
		public String getNextMessage()
		{
			String msg = "";
			if (!connected) { return msg; }

			lock (inMsgs)
			{
				if (inMsgs.Count() > 0)
				{
					msg = (String)inMsgs[0].Clone();
					inMsgs.RemoveAt(0);
				}
			}
			return msg;
		}

		/// <summary>
		/// Returns most recent message (LIFO) from incoming que and by default clears rest of que
		/// This is intended to be used like a udp stream (where only freshest data is important)
		/// </summary>
		/// <returns></returns>
		public string getLatestMessage(bool flush = true)
		{
			String msg = "";
			if (!connected) { return msg; }

			lock (inMsgs)
			{
				if (inMsgs.Count() > 0)
				{
					msg = inMsgs[inMsgs.Count() - 1];
					if (flush) { inMsgs.Clear(); }
					else { inMsgs.RemoveAt(inMsgs.Count() - 1); }
				}
			}
			return msg;
		}

		public int getUnreadMessageCount() { return inMsgs.Count(); }


		/// <summary>
		/// Sends a message to the connected socket
		/// </summary>
		/// <param name="message">message to be sent</param>
		/// <param name="asap"> if true will block until message is sent</param>
		/// <returns>returns true if message was sent successfully</returns>
		public bool sendMessage(String message, bool asap = false)
		{
			if (!connected) { return false; }

			lock (outMsgs)
			{
				if (asap)
				{
					try
					{
						message = message + delimChar;
						byte[] msgBytes;
						msgBytes = Encoding.UTF8.GetBytes(message);
						socStream.Write(msgBytes, 0, msgBytes.Length);
					}
					catch (Exception ex)
					{
						connected = false;
						if (debugPrints) { Console.WriteLine(ex.ToString()); }
						return false;
					}
				}
				else
				{
					outMsgs.Add(message);
				}
			}
			return true;
		}

		/// <summary>
		/// Sets up (and starts listening on) a server socket
		/// </summary>
		/// <param name="_port"></param>
		/// <param name="blocking">if true, function will not return until a client connects</param>
		/// <param name="autoReconnectOnDrop"> if true, socket will automatically reconnect if other side drops</param>
		public void serverConnect(int _port, bool blocking, bool autoReconnectOnDrop)
		{
			port = _port;
			autoReconnect = autoReconnectOnDrop;

			//sry for using this lamda syntax, ref here : https://stackoverflow.com/questions/1195896/threadstart-with-parameters
			connectionThread = new Thread(() => ConnectionManagement_DoWork(0));
			connectionThread.Start();

			if (blocking)
			{
				while (!connected)
				{
					Thread.Sleep(500);
				}
			}
		}

		/// <summary>
		/// Sets up client socket and attempts to connect to specified server
		/// </summary>
		/// <param name="_port"></param>
		/// <param name="_ip"></param>
		/// <param name="blocking">if true, function will not return until a client connects</param>
		/// <param name="autoReconnectOnDrop">if true, socket will automatically reconnect if other side drops</param>
		public void clientConnect(int _port, String _ip, bool blocking, bool autoReconnectOnDrop)
		{
			port = _port;
			ip = _ip;
			autoReconnect = autoReconnectOnDrop;

			//sry for using this syntax, ref here : https://stackoverflow.com/questions/1195896/threadstart-with-parameters
			connectionThread = new Thread(() => ConnectionManagement_DoWork(1));
			connectionThread.Start();

			if (blocking)
			{
				while (!connected)
				{
					Thread.Sleep(500);
				}
			}
		}

		private void ConnectionManagement_DoWork(int socType)
		{
			while (!connectionKillFlag)
			{
				if (!listening && !connected)
				{

					cleanupSoc();//kill any old socket members

					if (socType == 0) //server
					{
						serverConnect_Internal();
					}
					else //client
					{
						clientConnect_Internal();
					}
				}
				if (!autoReconnect) { break; } //end connection thread after one attempt
				Thread.Sleep(500);
			}
		}

		private void serverConnect_Internal()
		{
			listening = true;
			try
			{
				listener = new TcpListener(IPAddress.Any, port);
				listener.Start();
				connectedSocket = listener.AcceptTcpClient();
				socStream = connectedSocket.GetStream();
				connected = true;
			}
			catch (Exception ex)
			{
				if (debugPrints) { Console.WriteLine(ex.ToString()); }
				connected = false;
			}
			listening = false;
		}

		private void clientConnect_Internal()
		{
			listening = true;
			while (!connected && !connectionKillFlag)
			{
				try
				{
					// Stolen from: https://stackoverflow.com/questions/17118632/how-to-set-the-timeout-for-a-tcpclient
					connectedSocket = new TcpClient();
					var result = connectedSocket.BeginConnect(ip, port, null, null);
					var success = result.AsyncWaitHandle.WaitOne(TimeSpan.FromSeconds(1));
					if (success)
					{
						connectedSocket.EndConnect(result);
						socStream = connectedSocket.GetStream();
						connected = true;
					}
				}
				catch (Exception ex)
				{
					connected = false;
					if (debugPrints) { Console.WriteLine(ex.ToString()); }
				}
				Thread.Sleep(500);
			}
			listening = false;
		}

		private void cleanupSoc()
		{
			if (socStream != null)
			{
				socStream.Close();
				socStream.Dispose();
			}//.Dispose(); }

			if (listener != null)
			{ listener.Stop(); }

			if (connectedSocket != null)
			{ connectedSocket.Close(); }
		}

		private void SerialManagement_DoWork()
		{
			while (!socketManagementKillFlag)
			{

				//controls the socket update frequency
				//if less than 10ms period, hardlocks to make sure timing is hit as closely as possible
				//less than 10ms should only be used when completely neccesary as it will lock up a core.

				long curPeriod = mainClock.ElapsedMilliseconds;
				while (heartbeatPeriod > curPeriod)
				{
					if (heartbeatPeriod > 10)
					{
						Thread.Sleep((int)(heartbeatPeriod - curPeriod));
						break;
					}
					curPeriod = mainClock.ElapsedMilliseconds;
				}
				mainClock.Restart();
				//Thread.Sleep(heartbeatPeriod);

				if (!connected || listening) { continue; }

				try
				{
					//write outgoing messages or heartbeat if none
					lock (outMsgs)
					{
						byte[] msgBytes;

						if (outMsgs.Count() > 0)
						{
							String totalMsg = "";
							for (int i = 0; i < outMsgs.Count(); i++)
							{ totalMsg = totalMsg + outMsgs[i] + delimChar; }
							msgBytes = Encoding.UTF8.GetBytes(totalMsg);
							outMsgs.Clear();
						}
						else
						{
							msgBytes = Encoding.UTF8.GetBytes(delimChar);
						}

						socStream.Write(msgBytes, 0, msgBytes.Length);
					}

					//	// start debug
				}
				catch (Exception ex)
				{
					connected = false;
					Console.WriteLine("Send failed");
					if (debugPrints) { Console.WriteLine(ex.ToString()); }
				}
				try
				{

					//// end Debug

					//read incoming messages (ignoring heartbeats)
					lock (inMsgs)
					{
						Byte[] bytes = new Byte[4800000]; //256            
						String allAvailable = null;

						int i = socStream.Read(bytes, 0, bytes.Length);
						//Console.WriteLine(i);
						allAvailable += System.Text.Encoding.GetEncoding("437").GetString(bytes, 0, i); //Encoding.ASCII
						while (i == bytes.Length)
						{
							i = socStream.Read(bytes, 0, bytes.Length);
							allAvailable += System.Text.Encoding.GetEncoding("437").GetString(bytes, 0, i); //Encoding.ASCII
						}
						if (allAvailable != null)
						{
							List<string> toks = new List<string>();
							toks = allAvailable.Split(delimChar.ToCharArray()).ToList();

							for (int ii = 0; ii < toks.Count(); ii++)
							{
								if (toks[ii].Length > 0)
								{
									inMsgs.Add(toks[ii]);
								}
							}
						}
					}
				}
				catch (Exception ex)
				{
					connected = false;
					Console.WriteLine("recieve failed");
					if (debugPrints) { Console.WriteLine(ex.ToString()); }
				}
			}
		}


	}
}