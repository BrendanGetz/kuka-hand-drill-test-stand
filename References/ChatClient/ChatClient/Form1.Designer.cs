﻿namespace ChatClient
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.label1 = new System.Windows.Forms.Label();
			this.sendTB = new System.Windows.Forms.TextBox();
			this.receiveTB = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.clientBtn = new System.Windows.Forms.Button();
			this.serverBtn = new System.Windows.Forms.Button();
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.connectLbl = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(38, 39);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(62, 25);
			this.label1.TabIndex = 0;
			this.label1.Text = "Send";
			// 
			// sendTB
			// 
			this.sendTB.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.sendTB.Location = new System.Drawing.Point(43, 79);
			this.sendTB.Multiline = true;
			this.sendTB.Name = "sendTB";
			this.sendTB.Size = new System.Drawing.Size(326, 147);
			this.sendTB.TabIndex = 1;
			this.sendTB.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyPressDown);			
			// 
			// receiveTB
			// 
			this.receiveTB.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.receiveTB.Location = new System.Drawing.Point(476, 79);
			this.receiveTB.Multiline = true;
			this.receiveTB.Name = "receiveTB";
			this.receiveTB.Size = new System.Drawing.Size(409, 597);
			this.receiveTB.TabIndex = 3;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(471, 30);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(90, 25);
			this.label2.TabIndex = 2;
			this.label2.Text = "Receive";
			// 
			// clientBtn
			// 
			this.clientBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.clientBtn.Location = new System.Drawing.Point(43, 599);
			this.clientBtn.Name = "clientBtn";
			this.clientBtn.Size = new System.Drawing.Size(116, 66);
			this.clientBtn.TabIndex = 4;
			this.clientBtn.Text = "Client\r\nConnect";
			this.clientBtn.UseVisualStyleBackColor = true;
			this.clientBtn.Click += new System.EventHandler(this.clientBtn_Click);
			// 
			// serverBtn
			// 
			this.serverBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.serverBtn.Location = new System.Drawing.Point(253, 599);
			this.serverBtn.Name = "serverBtn";
			this.serverBtn.Size = new System.Drawing.Size(116, 66);
			this.serverBtn.TabIndex = 5;
			this.serverBtn.Text = "Server\r\nConnect";
			this.serverBtn.UseVisualStyleBackColor = true;
			this.serverBtn.Click += new System.EventHandler(this.serverBtn_Click);
			// 
			// timer1
			// 
			this.timer1.Enabled = true;
			this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
			// 
			// connectLbl
			// 
			this.connectLbl.AutoSize = true;
			this.connectLbl.BackColor = System.Drawing.Color.Khaki;
			this.connectLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.connectLbl.Location = new System.Drawing.Point(38, 512);
			this.connectLbl.Name = "connectLbl";
			this.connectLbl.Size = new System.Drawing.Size(155, 25);
			this.connectLbl.TabIndex = 6;
			this.connectLbl.Text = "Not Connected";
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.LightSkyBlue;
			this.ClientSize = new System.Drawing.Size(962, 718);
			this.Controls.Add(this.connectLbl);
			this.Controls.Add(this.serverBtn);
			this.Controls.Add(this.clientBtn);
			this.Controls.Add(this.receiveTB);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.sendTB);
			this.Controls.Add(this.label1);
			this.Name = "Form1";
			this.Text = "Form1";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox sendTB;
		private System.Windows.Forms.TextBox receiveTB;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button clientBtn;
		private System.Windows.Forms.Button serverBtn;
		private System.Windows.Forms.Timer timer1;
		private System.Windows.Forms.Label connectLbl;
	}
}

