package BrendanTesting;


import java.sql.Time;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import java.util.Calendar;
import javax.inject.Inject;
import javax.inject.Named;

import sun.security.action.GetLongAction;
import java.io.*;
import com.kuka.generated.ioAccess.Beckhoff_RemoteIOGroup;
import com.kuka.roboticsAPI.applicationModel.RoboticsAPIApplication;
import static com.kuka.roboticsAPI.motionModel.BasicMotions.*;

import com.kuka.roboticsAPI.conditionModel.ForceCondition;
import com.kuka.roboticsAPI.conditionModel.ICondition;
import com.kuka.roboticsAPI.deviceModel.JointEnum;
import com.kuka.roboticsAPI.deviceModel.JointPosition;
import com.kuka.roboticsAPI.deviceModel.LBR;
import com.kuka.roboticsAPI.executionModel.IFiredConditionInfo;
import com.kuka.roboticsAPI.geometricModel.AbstractFrame;
import com.kuka.roboticsAPI.geometricModel.CartDOF;
import com.kuka.roboticsAPI.geometricModel.CartPlane;
import com.kuka.roboticsAPI.geometricModel.Frame;
import com.kuka.roboticsAPI.geometricModel.ITransformationProvider;
import com.kuka.roboticsAPI.geometricModel.ObjectFrame;
import com.kuka.roboticsAPI.geometricModel.Tool;
import com.kuka.roboticsAPI.geometricModel.World;
import com.kuka.roboticsAPI.geometricModel.math.AxisAngleRotation;
import com.kuka.roboticsAPI.geometricModel.math.CoordinateAxis;
import com.kuka.roboticsAPI.geometricModel.math.IRotation;
import com.kuka.roboticsAPI.geometricModel.math.ITransformation;
import com.kuka.roboticsAPI.geometricModel.math.Rotation;
import com.kuka.roboticsAPI.geometricModel.math.Transformation;
import com.kuka.roboticsAPI.geometricModel.math.Vector;
import com.kuka.roboticsAPI.motionModel.IMotionContainer;
import com.kuka.roboticsAPI.motionModel.controlModeModel.CartesianSineImpedanceControlMode;
import com.kuka.roboticsAPI.sensorModel.ForceSensorData;
import com.kuka.roboticsAPI.uiModel.ApplicationDialogType;
import com.kuka.roboticsAPI.geometricModel.math.Matrix;

/**
 * Implementation of a robot application.
 * <p>
 * The application provides a {@link RoboticsAPITask#initialize()} and a 
 * {@link RoboticsAPITask#run()} method, which will be called successively in 
 * the application lifecycle. The application will terminate automatically after 
 * the {@link RoboticsAPITask#run()} method has finished or after stopping the 
 * task. The {@link RoboticsAPITask#dispose()} method will be called, even if an 
 * exception is thrown during initialization or run. 
 * <p>
 * <b>It is imperative to call <code>super.dispose()</code> when overriding the 
 * {@link RoboticsAPITask#dispose()} method.</b> 
 * 
 * @see UseRoboticsAPIContext
 * @see #initialize()
 * @see #run()
 * @see #dispose()
 */
public class handDrillTesting extends RoboticsAPIApplication 
{
	@Inject
	private LBR lBR;

	Frame workSurface;
	Frame surfaceOrigin;
	double workSurfaceLength_X;
	double workSurfaceLength_Y;
	String dataFileName;
	long timeStart;
	
	@Inject
	@Named("handDrill")
	private Tool handDrill;
	private Beckhoff_RemoteIOGroup remoteIO;
	
	@Override
	public void initialize() 
	{
		// initialize your application here
		SetAppHome();
		handDrill.attachTo(lBR.getFlange()); // Let iiwa know that there is a handDrill attached
		remoteIO = new Beckhoff_RemoteIOGroup(lBR.getController());
		workSurface = null;
		surfaceOrigin = null;
		dataFileName = null;
	}

	
	
	@Override
	public void run() 
	{
		// your application execution starts here

		handDrill.move(ptp(getApplicationData().getFrame("/handDrillHome")).setJointVelocityRel(0.1));
		
		//test
		int selection = -1;
		while(selection != 5)
		{
			if(workSurface == null || surfaceOrigin == null)
			{
				int choice = -1;
				choice = getApplicationUI().displayModalDialog(ApplicationDialogType.QUESTION, "Please select start or cancel.", 
					"findWorkSurface", "Cancel");
				switch(choice)
				{
					case 0:
						getLogger().info("findWorkSurface.");
						findWorkSurface();
						break;
					case 1:
						getLogger().info("Exiting.");
						break;
				}	
			}
			
			
			selection = getApplicationUI().displayModalDialog(ApplicationDialogType.QUESTION, "Please select start or cancel.", 
					"findWorkSurface", "cutSurface", "Case2", "Case3", "Case4", "Cancel");
			
			switch(selection)
			{
				case 0:
					getLogger().info("findWorkSurface.");
					findWorkSurface();
					break;
				case 1:
					getLogger().info("cutSurface.");
					cutSurface();
					break;
				case 2:
					getLogger().info("Case 2.");
					//surfaceFind();
					break;
				case 3:
					getLogger().info("Case 3.");
					//surfaceMove();
					break;
				case 4:
					getLogger().info("Case 4.");
					//complianceMode();
					break;
				case 5:
					getLogger().info("Exiting.");
					break;
			}	
		}
	}
	
	
	
	public void SetAppHome()
	{
		JointPosition newHome = new JointPosition(0, 0, 0, Math.toRadians(-90), 0, Math.toRadians(90), 0);
		lBR.setHomePosition(newHome);
	}
	
	
	////// DRILLL/////  potentially make drill class
	public void drillOn()
	{
		remoteIO.setOutput_01(true);
		remoteIO.setOutput_02(true);
	}	
	public void drillOff()
	{
		remoteIO.setOutput_01(false);
		remoteIO.setOutput_02(false);
	}
	public boolean drillStatus()
	{
		boolean status;
		if(remoteIO.getOutput_01() || remoteIO.getOutput_02()){status = true;}
		else{status = false;}
		return status;
	}
	
	
	/////// SLEEP //////
	public void sleep(int time_ms)
	{
		try 
		{
			Thread.sleep(time_ms);
		} catch (InterruptedException e) 
		{
			e.printStackTrace();
		}
	}
	
	//////////// Cut Surface into the Auto Generate Cut Area //////////////
	public void cutSurface()	//three holes
	{	
		dataFileName = "VelocityTest " + Calendar.YEAR + "-" + Calendar.MONTH + "-" + Calendar.DAY_OF_MONTH + "(" + Calendar.HOUR_OF_DAY + "." + Calendar.MINUTE + ")";
		
		// assume starting at origin of workSurface
		double holeDepth = 10.0;
		double holeSpeed = 5.0;
		double lineDepth = 9.0;
		double lineSpeed = 5.0;
		double lineLength = 75.0;
		int linePasses = 3;
		double safeHeight = 5.0;
		
		handDrill.move(ptp(surfaceOrigin).setJointVelocityRel(0.1));
		timeStart = System.currentTimeMillis();
		
		handDrill.move(linRel(4,4,safeHeight,0,0,0, workSurface).setCartVelocity(30));
		handDrill.move(linRel(0,-4,0,0,0,0, workSurface).setCartVelocity(30));
		sleep(3000);
		handDrill.move(linRel(0,4,0,0,0,0, workSurface).setCartVelocity(30));
		
		Frame testStartPos = lBR.getCurrentCartesianPosition(handDrill.getDefaultMotionFrame());
		double distMoved_X = 0;
		int loopsCompleted = 0;
		while(distMoved_X < workSurfaceLength_X - 8)
		{
			//one line, two holes
			if(safeHeight < 1.0 || safeHeight > 10)
			{safeHeight = 5;}
			
			Frame startPos = lBR.getCurrentCartesianPosition(handDrill.getDefaultMotionFrame());

			drillOn();
			sleep(1000);
			lineCutSpeed(lineLength, lineDepth, lineSpeed, linePasses, safeHeight);
			handDrill.move(linRel(0,6,0,0,0,0, workSurface).setCartVelocity(50));
			
			holePlungeSpeed(holeDepth, holeSpeed, safeHeight);
			handDrill.move(linRel(0,6,0,0,0,0, workSurface).setCartVelocity(50));
			
			holePlungeSpeed(holeDepth, holeSpeed, safeHeight);
			sleep(1000);
			drillOff();
			
			Frame endPos = lBR.getCurrentCartesianPosition(handDrill.getDefaultMotionFrame());
			Vector distToStart = vectorFrameToFrame(endPos, startPos);
			handDrill.move(linRel(distToStart.getX(),distToStart.getY(),distToStart.getZ(),0,0,0, World.Current.getRootFrame()).setCartVelocity(50));	

			
			handDrill.move(linRel(5,0,0,0,0,0, workSurface).setCartVelocity(50));		
			Frame currentPos = lBR.getCurrentCartesianPosition(handDrill.getDefaultMotionFrame());
			distMoved_X = vectorFrameToFrame(currentPos, testStartPos).length();
			loopsCompleted++;
			getLogger().info(loopsCompleted + " loops completed");
			sleep(10000);
		}
	}
	
	
	private void holePlungeSpeed(double depth, double speed, double safeHeight)
	{	
		getLogger().info("hole plunge");		
		//set within limits
		if(speed < 0.5){speed = 0.5;}
		if(speed > 15) {speed = 15;}		
		if(depth < 0.5){depth = 0.5;}
		if(depth > 15) {speed = 15;}
		
		handDrill.moveAsync(linRel(0,0,-(depth+safeHeight),0,0,0, workSurface).setCartVelocity(speed));
		recordAverageAndMaxForces(safeHeight - 2, depth - 1);
		handDrill.move(linRel(0,0,depth+safeHeight,0,0,0, workSurface).setCartVelocity(2*speed));
	}
	
	
	private void lineCutSpeed(double length, double depth, double speed, int numPasses, double safeHeight)
	{
		getLogger().info("linear cut");
		handDrill.move(linRel(0,0,-safeHeight,0,0,0, workSurface).setCartVelocity(speed));
		
		for(int i = 1; i <= numPasses; i++)
		{
			handDrill.move(linRel(0,0,-depth/numPasses,0,0,0, workSurface).setCartVelocity(speed));
			if(i % 2 == 1)
			{
				handDrill.moveAsync(linRel(0,length,0,0,0,0, workSurface).setCartVelocity(speed));
				getLogger().info("pass number " + i);
				recordAverageAndMaxForces(2, length - 2);
			}			
			if(i % 2 == 0)
			{
				handDrill.moveAsync(linRel(0,-length,0,0,0,0, workSurface).setCartVelocity(speed));
				getLogger().info("pass number " + i);
				recordAverageAndMaxForces(2, length - 2);
			}
		}
		handDrill.move(linRel(0,0,safeHeight+depth,0,0,0, workSurface).setCartVelocity(speed));
		
		
		if(numPasses % 2 == 1)
		{
			handDrill.move(linRel(0,0,0,0,0,0, workSurface).setCartVelocity(2*speed));
		}			
		if(numPasses % 2 == 0)
		{
			handDrill.move(linRel(0,length,0,0,0,0, workSurface).setCartVelocity(2*speed));
		}
	}
	
	
	public void printRobotStatus()
	{
		Frame myPositionInfo = lBR.getCurrentCartesianPosition(handDrill.getDefaultMotionFrame());
		//Vector myVector
		//JointPosition myRotInfo = lBR.getCurrentJointPosition();
		double x = myPositionInfo.getX();
		double y = myPositionInfo.getY();
		double z = myPositionInfo.getZ();
		//double a = myRotInfo.get(JointEnum.J1);
		//double b = myRotInfo.get(JointEnum.J2);
		//double c = myRotInfo.get(JointEnum.J3);
		
		
		ForceSensorData forces = lBR.getExternalForceTorque(handDrill.getDefaultMotionFrame());
		Vector force = forces.getForce();

		double xforce = force.getX();
		double yforce = force.getY();
		double zforce = force.getZ();		
		
		getLogger().info("Printing position and force information...");
		getLogger().info("X pos: " + x +" Y pos: " + y + " Z pos: " + z );
		getLogger().info("X force: " + xforce +" Y force: "+ yforce + " Z force: " +zforce );
	}
	
	public String vectorToString(Vector in)
	{
		String out = in.getX() + "," + in.getY() + "," + in.getZ();
		return out;
	}	
	public String frameToString(Frame in)
	{
		String out = in.getX() + "," + in.getY() + "," + in.getZ();
		return out;
	}
	
	public void recordAverageAndMaxForces(double startDistance, double endDistance)
	{
		try 
		{
			File file = new File ("E:\\DrillTesting\\" + dataFileName + ".csv");
			FileWriter fw;
			PrintWriter pw;
			
			fw = new FileWriter(file, true);
			pw = new PrintWriter(fw);
			
			Frame startPos = lBR.getCurrentCartesianPosition(handDrill.getDefaultMotionFrame());
			Frame currentPos = lBR.getCurrentCartesianPosition(handDrill.getDefaultMotionFrame());
			double traveled = vectorFrameToFrame(startPos, currentPos).length();
			Vector vectorAccumulator = Vector.of(0,0,0);
			double lengthAccumulator = 0;
			int sampleCount = 0;
			double maxForce_X = 0;
			double maxForce_Y = 0;
			double maxForce_Z = 0;
			Vector maxInstantForce = Vector.of(0,0,0);
	
			//Don't record until start distance completed
			while(traveled < startDistance)
			{	
				currentPos = lBR.getCurrentCartesianPosition(handDrill.getDefaultMotionFrame());
				traveled = vectorFrameToFrame(startPos, currentPos).length();
				sleep(10);
			}
	
			//Record until the robot has reached end distance
			while(traveled < endDistance)
			{
				long timeCurrent = System.currentTimeMillis() - timeStart;
				currentPos = lBR.getCurrentCartesianPosition(handDrill.getDefaultMotionFrame());
				ForceSensorData forces = lBR.getExternalForceTorque(handDrill.getDefaultMotionFrame());
				Vector force = forces.getForce();					
				
				if(Math.abs(force.getX()) > Math.abs(maxForce_X))
				{
					maxForce_X = force.getX();
				}					
				if(Math.abs(force.getY()) > Math.abs(maxForce_Y))
				{
					maxForce_Y = force.getY();
				}
				if(Math.abs(force.getZ()) > Math.abs(maxForce_Z))
				{
					maxForce_Z = force.getZ();
				}
				if(force.length() > maxInstantForce.length())
				{
					maxInstantForce = force;
				}
				
				String forceString = vectorToString(force);
				String posString = frameToString(currentPos);
				pw.println(timeCurrent + "," + posString + "," + forceString);
				
				vectorAccumulator = vectorAccumulator.add(force);
				lengthAccumulator += force.length();
				sampleCount++;
	
				traveled = vectorFrameToFrame(startPos, currentPos).length();
	
				sleep(10);
			}
			
			Vector averageVectorForce = Vector.of(vectorAccumulator.getX()/sampleCount, vectorAccumulator.getY()/sampleCount, vectorAccumulator.getZ()/sampleCount);
			double averageLengthForce = lengthAccumulator/sampleCount;
			
			getLogger().info("Max Force: + \n" +  "X: " + maxForce_X + "\n" + "Y: " + maxForce_Y + "\n" + "Z: " + maxForce_Z);
			getLogger().info("Max Instantaneous: " + maxInstantForce + "\n" + "Magnitude: " + maxInstantForce.length());
			getLogger().info("Average Force: \n" + "X = " + averageVectorForce.getX() + "\n" + "Y = " + averageVectorForce.getY() + "\n" + "Z = " + averageVectorForce.getZ());
			getLogger().info("Average Magnitude: " + averageLengthForce);
			
			pw.println();
			pw.println();
			pw.close();
		} 
		catch (IOException e) 
		{
			getLogger().error("ERROR STORING DATA TO USB");
			
			Frame startPos = lBR.getCurrentCartesianPosition(handDrill.getDefaultMotionFrame());
			Frame currentPos = lBR.getCurrentCartesianPosition(handDrill.getDefaultMotionFrame());
			double traveled = vectorFrameToFrame(startPos, currentPos).length();
			Vector vectorAccumulator = Vector.of(0,0,0);
			double lengthAccumulator = 0;
			int sampleCount = 0;
			double maxForce_X = 0;
			double maxForce_Y = 0;
			double maxForce_Z = 0;
			Vector maxInstantForce = Vector.of(0,0,0);
	
			//Don't record until start distance completed
			while(traveled < startDistance)
			{	
				currentPos = lBR.getCurrentCartesianPosition(handDrill.getDefaultMotionFrame());
				traveled = vectorFrameToFrame(startPos, currentPos).length();
				sleep(10);
			}
	
			//Record until the robot has reached end distance
			while(traveled < endDistance)
			{
				currentPos = lBR.getCurrentCartesianPosition(handDrill.getDefaultMotionFrame());
				ForceSensorData forces = lBR.getExternalForceTorque(handDrill.getDefaultMotionFrame());
				Vector force = forces.getForce();					
				
				if(Math.abs(force.getX()) > Math.abs(maxForce_X))
				{
					maxForce_X = force.getX();
				}					
				if(Math.abs(force.getY()) > Math.abs(maxForce_Y))
				{
					maxForce_Y = force.getY();
				}
				if(Math.abs(force.getZ()) > Math.abs(maxForce_Z))
				{
					maxForce_Z = force.getZ();
				}
				if(force.length() > maxInstantForce.length())
				{
					maxInstantForce = force;
				}
				
				vectorAccumulator = vectorAccumulator.add(force);
				lengthAccumulator += force.length();
				sampleCount++;
	
				traveled = vectorFrameToFrame(startPos, currentPos).length();
	
				sleep(10);
			}
			
			Vector averageVectorForce = Vector.of(vectorAccumulator.getX()/sampleCount, vectorAccumulator.getY()/sampleCount, vectorAccumulator.getZ()/sampleCount);
			double averageLengthForce = lengthAccumulator/sampleCount;
			
			getLogger().info("Max Force: + \n" +  "X: " + maxForce_X + "\n" + "Y: " + maxForce_Y + "\n" + "Z: " + maxForce_Z);
			getLogger().info("Max Instantaneous: " + maxInstantForce + "\n" + "Magnitude: " + maxInstantForce.length());
			getLogger().info("Average Force: \n" + "X = " + averageVectorForce.getX() + "\n" + "Y = " + averageVectorForce.getY() + "\n" + "Z = " + averageVectorForce.getZ());
			getLogger().info("Average Magnitude: " + averageLengthForce);
			
			e.printStackTrace();
		}
	}
	
	
	public Vector vectorFrameToFrame(Frame A, Frame B)
	{
		Vector AB = Vector.of(B.getX() - A.getX(), B.getY() - A.getY(), B.getZ() - A.getZ());
		return AB;
	}
	
	
	public Frame planeFrameCalc(Frame origin, Vector xAxis, Vector yAxis)
	{
		getLogger().info("Calculating Plane...");	
		
		xAxis = xAxis.normalize();
		yAxis = yAxis.normalize();
		Vector zAxis = xAxis.crossProduct(yAxis).normalize();
		yAxis = zAxis.crossProduct(xAxis).normalize();
		
		
		////////////////// Defined Axis' Method ////////////////
		Transformation sufaceTransform = Transformation.of(Vector.of(origin.getX(),origin.getY(),origin.getZ()), Matrix.ofColumns(xAxis, yAxis, zAxis));
		
		AbstractFrame abstractPointA = origin.copyWithRedundancy(World.Current.getRootFrame());		
		Frame surfaceCreated = new Frame(abstractPointA.copyWithRedundancy(), sufaceTransform);
	
		
		// print the new surface
		getLogger().info("surfaceCreated Pos: " + surfaceCreated.getX() + "," + surfaceCreated.getY() + "," + surfaceCreated.getZ());
		getLogger().info("surfaceCreated Rot: " + surfaceCreated.getAlphaRad() * 180 / Math.PI + "," + surfaceCreated.getBetaRad() * 180 / Math.PI + "," + surfaceCreated.getGammaRad() * 180 / Math.PI);

		return surfaceCreated;
	}	
	

	public Vector skewMidpoint(Frame nearPoint0, Frame farPoint0, Frame nearPoint1, Frame farPoint1)
	{
		//Returns skew midpoint in XYZ coordinates
		//See this as reference for the equations:
		//https://en.wikipedia.org/wiki/Skew_lines#Distance
	
		Vector p1 = Vector.of(nearPoint0.getX(), nearPoint0.getY(), nearPoint0.getZ());
		Vector d1 = Vector.of(farPoint0.getX() - nearPoint0.getX(), farPoint0.getY() - nearPoint0.getY(), farPoint0.getZ() - nearPoint0.getZ());
		d1.normalize();
		Vector p2 = Vector.of(nearPoint1.getX(), nearPoint1.getY(), nearPoint1.getZ());
		Vector d2 = Vector.of(farPoint1.getX() - nearPoint1.getX(), farPoint1.getY() - nearPoint1.getY(), farPoint1.getZ() - nearPoint1.getZ());
		d2.normalize();
	
		Vector n2 = d2.crossProduct(d1.crossProduct(d2));
		Vector n1 = d1.crossProduct(d2.crossProduct(d1));
	
		double p1Scalar = ((Vector.of(p2.getX() - p1.getX(), p2.getY() - p1.getY(), p2.getZ() - p1.getZ()).dotProduct(n2)) / (d1.dotProduct(n2)));
		double p2Scalar = ((Vector.of(p1.getX() - p2.getX(), p1.getY() - p2.getY(), p1.getZ() - p2.getZ()).dotProduct(n1)) / (d2.dotProduct(n1)));
		Vector closestPt1 = Vector.of(p1.getX() + (p1Scalar * d1.getX()), p1.getY() + (p1Scalar * d1.getY()), p1.getZ() + (p1Scalar * d1.getZ()));
		Vector closestPt2 = Vector.of(p2.getX() + (p2Scalar * d2.getX()), p2.getY() + (p2Scalar * d2.getY()), p2.getZ() + (p1Scalar * d2.getZ()));
	
		Vector midPoint = Vector.of((closestPt1.getX() + closestPt2.getX()) / 2.0, (closestPt1.getY() + closestPt2.getY()) / 2.0, (closestPt1.getZ() + closestPt2.getZ()) / 2.0);
	
		return midPoint;
	}
	
	
	public void findWorkSurface()
	{	
		Frame surfaceStart = null;	//initial contact point
		Frame point_A = null;		//used to generate x axis
		Frame point_B = null;		//used to generate x axis
		Frame point_C = null;		//used to generate y axis
		Frame point_D = null;		//used to generate y axis
		Frame point_E = null;		//move to find, used to generate test surface limits
		Frame point_F = null;		//move to find, used to generate test surface limits
		
		double searchForce = getApplicationData().getProcessData("searchForce").getValue();
		double searchSpeed = getApplicationData().getProcessData("searchSpeed").getValue();
		
		getLogger().info("Moving to handDrillStart");
		handDrill.move(ptp(getApplicationData().getFrame("/handDrillStart")).setJointVelocityRel(0.1));
		getLogger().info("At handDrillStart");
		
		getLogger().info("Looking for Block");
		ForceCondition tableForce = ForceCondition.createSpatialForceCondition(handDrill.getDefaultMotionFrame(), searchForce);
		IMotionContainer tableFindContainer = handDrill.move(linRel(0,0,-100,0,0,0, World.Current.getRootFrame()).breakWhen(tableForce).setCartVelocity(searchSpeed).setCartAcceleration(10));
		IFiredConditionInfo tableForceFireCond = tableFindContainer.getFiredBreakConditionInfo();
		
		Frame currentPosition = lBR.getCurrentCartesianPosition(handDrill.getDefaultMotionFrame());
		
		if(tableForceFireCond != null)
		{	
			getLogger().info("Block Found");
			surfaceStart = currentPosition.copyWithRedundancy();
			handDrill.move(linRel(0,0,5,0,0,0, World.Current.getRootFrame()).setJointVelocityRel(0.1));
		} 			

		if(tableForceFireCond == null)
		{
			getLogger().error("Can't find Block!!!");
			return;
		}
		
		int surfaceMaxX = 75; // units of mm
		int surfaceMaxY = 150; // units of mm
		Frame aboveStart = surfaceStart.copyWithRedundancy();
		aboveStart.setZ(surfaceStart.getZ()+5);
		
		ForceCondition surfaceHeight = ForceCondition.createSpatialForceCondition(handDrill.getDefaultMotionFrame(), searchForce);
		IMotionContainer detectForceContainer;
		IFiredConditionInfo detectForceFireCond;
		
		// Points for X Axis
		handDrill.move(ptp(aboveStart).setJointVelocityRel(0.1));
		
		///////////   Establish Point A   ///////////////
		handDrill.move(linRel(-25,-surfaceMaxY,0,0,0,0, World.Current.getRootFrame()).setJointVelocityRel(0.1));
		handDrill.move(linRel(0,0,-10,0,0,0, World.Current.getRootFrame()).setJointVelocityRel(0.1));
		detectForceContainer = handDrill.move(linRel(0,surfaceMaxY,0,0,0,0, World.Current.getRootFrame()).breakWhen(surfaceHeight).setCartVelocity(searchSpeed).setCartAcceleration(10));
		detectForceFireCond = detectForceContainer.getFiredBreakConditionInfo();
		
		if(detectForceFireCond != null) //for side
		{
			getLogger().info("Side Found");
			handDrill.move(linRel(0,-3,0,0,0,0, World.Current.getRootFrame()).setJointVelocityRel(0.1));
			handDrill.move(linRel(0,0,10,0,0,0, World.Current.getRootFrame()).setJointVelocityRel(0.1));
			handDrill.move(linRel(0,6,0,0,0,0, World.Current.getRootFrame()).setJointVelocityRel(0.1));
			
			detectForceContainer = handDrill.move(linRel(0,0,-10,0,0,0, World.Current.getRootFrame()).breakWhen(surfaceHeight).setCartVelocity(searchSpeed).setCartAcceleration(10));
			detectForceFireCond = detectForceContainer.getFiredBreakConditionInfo();
			
			if(detectForceFireCond != null)  //for limit on top surface
			{		
				getLogger().info("A Found");
				point_A = lBR.getCurrentCartesianPosition(handDrill.getDefaultMotionFrame());

				handDrill.move(linRel(0,0,5,0,0,0, World.Current.getRootFrame()).setJointVelocityRel(0.1));
			} 			

			if(detectForceFireCond == null) //failed on top surface
			{
				getLogger().error("Top Surface NOT Found!!!");
				handDrill.move(linRel(0,0,100,0,0,0, World.Current.getRootFrame()).setJointVelocityRel(0.1));
				handDrill.move(ptp(getApplicationData().getFrame("/handDrillStart")).setJointVelocityRel(0.1));
				return;
			}
		} 			

		if(detectForceFireCond == null)//failed finding side
		{
			getLogger().error("Side NOT Found!!!");
			handDrill.move(linRel(0,0,100,0,0,0, World.Current.getRootFrame()).setJointVelocityRel(0.1));
			handDrill.move(ptp(getApplicationData().getFrame("/handDrillStart")).setJointVelocityRel(0.1));
			return;
		}
		
		
		///////////   Establish Point B   ///////////////
		handDrill.move(linRel(50,-15,0,0,0,0, World.Current.getRootFrame()).setJointVelocityRel(0.1));
		handDrill.move(linRel(0,0,-10,0,0,0, World.Current.getRootFrame()).setJointVelocityRel(0.1));
		detectForceContainer = handDrill.move(linRel(0,surfaceMaxY,0,0,0,0, World.Current.getRootFrame()).breakWhen(surfaceHeight).setCartVelocity(searchSpeed).setCartAcceleration(10));
		detectForceFireCond = detectForceContainer.getFiredBreakConditionInfo();
		
		if(detectForceFireCond != null) //for side
		{
			getLogger().info("Side Found");
			handDrill.move(linRel(0,-3,0,0,0,0, World.Current.getRootFrame()).setJointVelocityRel(0.1));
			handDrill.move(linRel(0,0,10,0,0,0, World.Current.getRootFrame()).setJointVelocityRel(0.1));
			handDrill.move(linRel(0,6,0,0,0,0, World.Current.getRootFrame()).setJointVelocityRel(0.1));
			
			detectForceContainer = handDrill.move(linRel(0,0,-10,0,0,0, World.Current.getRootFrame()).breakWhen(surfaceHeight).setCartVelocity(searchSpeed).setCartAcceleration(10));
			detectForceFireCond = detectForceContainer.getFiredBreakConditionInfo();
			
			if(detectForceFireCond != null)  //for limit on top surface
			{		
				getLogger().info("B Found");
				point_B = lBR.getCurrentCartesianPosition(handDrill.getDefaultMotionFrame());

				handDrill.move(linRel(0,0,5,0,0,0, World.Current.getRootFrame()).setJointVelocityRel(0.1));
			} 			

			if(detectForceFireCond == null) //failed on top surface
			{
				getLogger().error("Top Surface NOT Found!!!");
				handDrill.move(linRel(0,0,100,0,0,0, World.Current.getRootFrame()).setJointVelocityRel(0.1));
				handDrill.move(ptp(getApplicationData().getFrame("/handDrillStart")).setJointVelocityRel(0.1));
				return;
			}
		} 			

		if(detectForceFireCond == null)//failed finding side
		{
			getLogger().error("Side NOT Found!!!");
			handDrill.move(linRel(0,0,100,0,0,0, World.Current.getRootFrame()).setJointVelocityRel(0.1));
			handDrill.move(ptp(getApplicationData().getFrame("/handDrillStart")).setJointVelocityRel(0.1));
			return;
		}
		
		
		// Points for Y Axis
		handDrill.move(ptp(aboveStart).setJointVelocityRel(0.1));
		
		///////////   Establish Point C   ///////////////
		handDrill.move(linRel(-surfaceMaxX,-30,0,0,0,0, World.Current.getRootFrame()).setJointVelocityRel(0.1));
		handDrill.move(linRel(0,0,-10,0,0,0, World.Current.getRootFrame()).setJointVelocityRel(0.1));
		detectForceContainer = handDrill.move(linRel(surfaceMaxX,0,0,0,0,0, World.Current.getRootFrame()).breakWhen(surfaceHeight).setCartVelocity(searchSpeed).setCartAcceleration(10));
		detectForceFireCond = detectForceContainer.getFiredBreakConditionInfo();
		
		if(detectForceFireCond != null) //for side
		{
			getLogger().info("Side Found");
			handDrill.move(linRel(-3,0,0,0,0,0, World.Current.getRootFrame()).setJointVelocityRel(0.1));
			handDrill.move(linRel(0,0,10,0,0,0, World.Current.getRootFrame()).setJointVelocityRel(0.1));
			handDrill.move(linRel(6,0,0,0,0,0, World.Current.getRootFrame()).setJointVelocityRel(0.1));
			
			detectForceContainer = handDrill.move(linRel(0,0,-10,0,0,0, World.Current.getRootFrame()).breakWhen(surfaceHeight).setCartVelocity(searchSpeed).setCartAcceleration(10));
			detectForceFireCond = detectForceContainer.getFiredBreakConditionInfo();
			
			if(detectForceFireCond != null)  //for limit on top surface
			{		
				getLogger().info("C Found");
				point_C = lBR.getCurrentCartesianPosition(handDrill.getDefaultMotionFrame());

				handDrill.move(linRel(0,0,5,0,0,0, World.Current.getRootFrame()).setJointVelocityRel(0.1));
			} 			

			if(detectForceFireCond == null) //failed on top surface
			{
				getLogger().error("Top Surface NOT Found!!!");
				handDrill.move(linRel(0,0,100,0,0,0, World.Current.getRootFrame()).setJointVelocityRel(0.1));
				handDrill.move(ptp(getApplicationData().getFrame("/handDrillStart")).setJointVelocityRel(0.1));
				return;
			}
		} 			

		if(detectForceFireCond == null)//failed finding side
		{
			getLogger().error("Side NOT Found!!!");
			handDrill.move(linRel(0,0,100,0,0,0, World.Current.getRootFrame()).setJointVelocityRel(0.1));
			handDrill.move(ptp(getApplicationData().getFrame("/handDrillStart")).setJointVelocityRel(0.1));
			return;
		}
		
		
		///////////   Establish Point D   ///////////////
		handDrill.move(linRel(-15,60,0,0,0,0, World.Current.getRootFrame()).setJointVelocityRel(0.1));
		handDrill.move(linRel(0,0,-10,0,0,0, World.Current.getRootFrame()).setJointVelocityRel(0.1));
		detectForceContainer = handDrill.move(linRel(surfaceMaxX,0,0,0,0,0, World.Current.getRootFrame()).breakWhen(surfaceHeight).setCartVelocity(searchSpeed).setCartAcceleration(10));
		detectForceFireCond = detectForceContainer.getFiredBreakConditionInfo();
		
		if(detectForceFireCond != null) //for side
		{
			getLogger().info("Side Found");
			handDrill.move(linRel(-3,0,0,0,0,0, World.Current.getRootFrame()).setJointVelocityRel(0.1));
			handDrill.move(linRel(0,0,10,0,0,0, World.Current.getRootFrame()).setJointVelocityRel(0.1));
			handDrill.move(linRel(6,0,0,0,0,0, World.Current.getRootFrame()).setJointVelocityRel(0.1));
			
			detectForceContainer = handDrill.move(linRel(0,0,-10,0,0,0, World.Current.getRootFrame()).breakWhen(surfaceHeight).setCartVelocity(searchSpeed).setCartAcceleration(10));
			detectForceFireCond = detectForceContainer.getFiredBreakConditionInfo();
			
			if(detectForceFireCond != null)  //for limit on top surface
			{		
				getLogger().info("D Found");
				point_D = lBR.getCurrentCartesianPosition(handDrill.getDefaultMotionFrame());

				handDrill.move(linRel(0,0,5,0,0,0, World.Current.getRootFrame()).setJointVelocityRel(0.1));
			} 			

			if(detectForceFireCond == null) //failed on top surface
			{
				getLogger().error("Top Surface NOT Found!!!");
				handDrill.move(linRel(0,0,100,0,0,0, World.Current.getRootFrame()).setJointVelocityRel(0.1));
				handDrill.move(ptp(getApplicationData().getFrame("/handDrillStart")).setJointVelocityRel(0.1));
				return;
			}
		} 			

		if(detectForceFireCond == null)//failed finding side
		{
			getLogger().error("Side NOT Found!!!");
			handDrill.move(linRel(0,0,100,0,0,0, World.Current.getRootFrame()).setJointVelocityRel(0.1));
			handDrill.move(ptp(getApplicationData().getFrame("/handDrillStart")).setJointVelocityRel(0.1));
			return;
		}
		
		Vector originXYZ = skewMidpoint(point_A, point_B, point_C, point_D);
		surfaceOrigin = surfaceStart.copyWithRedundancy();
		surfaceOrigin.setX(originXYZ.getX());
		surfaceOrigin.setY(originXYZ.getY());
		surfaceOrigin.setZ(originXYZ.getZ());
		
		//get axis directions
		Vector xAxisDir = vectorFrameToFrame(point_A, point_B);
		Vector yAxisDir = vectorFrameToFrame(point_C, point_D);

		//generate work surface
		workSurface = planeFrameCalc(surfaceOrigin, xAxisDir, yAxisDir).copyWithRedundancy();
		getLogger().info("Work Surface generated and stored to Frame handDrillWorkSurface");
		
		
		// Measurements for length of X Work Space
		handDrill.move(ptp(point_C).setJointVelocityRel(0.1));
		handDrill.move(linRel(0,0,5,0,0,0, workSurface).setJointVelocityRel(0.1));
		
		///////////   Establish Point E   ///////////////
		double xLengthCheck = 150;
		handDrill.move(linRel(xLengthCheck,0,0,0,0,0, workSurface).setJointVelocityRel(0.1));
		handDrill.move(linRel(0,0,-10,0,0,0, workSurface).setJointVelocityRel(0.1));
		detectForceContainer = handDrill.move(linRel(-xLengthCheck,0,0,0,0,0, workSurface).breakWhen(surfaceHeight).setCartVelocity(searchSpeed).setCartAcceleration(10));
		detectForceFireCond = detectForceContainer.getFiredBreakConditionInfo();
		
		if(detectForceFireCond != null) //for side
		{
			getLogger().info("Side Found");
			handDrill.move(linRel(3,0,0,0,0,0, workSurface).setJointVelocityRel(0.1));
			handDrill.move(linRel(0,0,10,0,0,0, workSurface).setJointVelocityRel(0.1));
			handDrill.move(linRel(-6,0,0,0,0,0, workSurface).setJointVelocityRel(0.1));
			
			detectForceContainer = handDrill.move(linRel(0,0,-10,0,0,0, workSurface).breakWhen(surfaceHeight).setCartVelocity(searchSpeed).setCartAcceleration(10));
			detectForceFireCond = detectForceContainer.getFiredBreakConditionInfo();
			
			if(detectForceFireCond != null)  //for limit on top surface
			{		
				getLogger().info("E Found");
				point_E = lBR.getCurrentCartesianPosition(handDrill.getDefaultMotionFrame());

				handDrill.move(linRel(0,0,5,0,0,0, workSurface).setJointVelocityRel(0.1));
			} 			

			if(detectForceFireCond == null) //failed on top surface
			{
				getLogger().error("Top Surface NOT Found!!!");
				handDrill.move(linRel(0,0,100,0,0,0, World.Current.getRootFrame()).setJointVelocityRel(0.1));
				handDrill.move(ptp(getApplicationData().getFrame("/handDrillStart")).setJointVelocityRel(0.1));
				return;
			}
		} 			

		if(detectForceFireCond == null)//failed finding side
		{
			getLogger().error("Side NOT Found!!!");
			handDrill.move(linRel(0,0,100,0,0,0, World.Current.getRootFrame()).setJointVelocityRel(0.1));
			handDrill.move(ptp(getApplicationData().getFrame("/handDrillStart")).setJointVelocityRel(0.1));
			return;
		}
		
		
		// Measurements for length of Y Work Space
		handDrill.move(ptp(point_A).setJointVelocityRel(0.1));
		handDrill.move(linRel(0,0,5,0,0,0, workSurface).setJointVelocityRel(0.1));
		
		///////////   Establish Point F   ///////////////
		double yLengthCheck = 275;
		handDrill.move(linRel(0,yLengthCheck,0,0,0,0, workSurface).setJointVelocityRel(0.1));
		handDrill.move(linRel(0,0,-10,0,0,0, workSurface).setJointVelocityRel(0.1));
		detectForceContainer = handDrill.move(linRel(0,-yLengthCheck,0,0,0,0, workSurface).breakWhen(surfaceHeight).setCartVelocity(searchSpeed).setCartAcceleration(10));
		detectForceFireCond = detectForceContainer.getFiredBreakConditionInfo();
		
		if(detectForceFireCond != null) //for side
		{
			getLogger().info("Side Found");
			handDrill.move(linRel(0,3,0,0,0,0, workSurface).setJointVelocityRel(0.1));
			handDrill.move(linRel(0,0,10,0,0,0, workSurface).setJointVelocityRel(0.1));
			handDrill.move(linRel(0,-6,0,0,0,0, workSurface).setJointVelocityRel(0.1));
			
			detectForceContainer = handDrill.move(linRel(0,0,-10,0,0,0, workSurface).breakWhen(surfaceHeight).setCartVelocity(searchSpeed).setCartAcceleration(10));
			detectForceFireCond = detectForceContainer.getFiredBreakConditionInfo();
			
			if(detectForceFireCond != null)  //for limit on top surface
			{		
				getLogger().info("F Found");
				point_F = lBR.getCurrentCartesianPosition(handDrill.getDefaultMotionFrame());

				handDrill.move(linRel(0,0,5,0,0,0, workSurface).setJointVelocityRel(0.1));
			} 			

			if(detectForceFireCond == null) //failed on top surface
			{
				getLogger().error("Top Surface NOT Found!!!");
				handDrill.move(linRel(0,0,100,0,0,0, World.Current.getRootFrame()).setJointVelocityRel(0.1));
				handDrill.move(ptp(getApplicationData().getFrame("/handDrillStart")).setJointVelocityRel(0.1));
				return;
			}
		} 			

		if(detectForceFireCond == null)//failed finding side
		{
			getLogger().error("Side NOT Found!!!");
			handDrill.move(linRel(0,0,100,0,0,0, World.Current.getRootFrame()).setJointVelocityRel(0.1));
			handDrill.move(ptp(getApplicationData().getFrame("/handDrillStart")).setJointVelocityRel(0.1));
			return;
		}
		
		workSurfaceLength_X = vectorFrameToFrame(point_C, point_E).length();
		workSurfaceLength_Y = vectorFrameToFrame(point_A, point_F).length();
		

		handDrill.move(ptp(aboveStart).setJointVelocityRel(0.1));
		
		//test work surface
		handDrill.move(ptp(surfaceOrigin).setJointVelocityRel(0.1));
		handDrill.move(linRel(0,0,2,0,0,0, workSurface).setJointVelocityRel(0.1));
		handDrill.move(linRel(workSurfaceLength_X,0,0,0,0,0, workSurface).setJointVelocityRel(0.1));
		handDrill.move(linRel(0,workSurfaceLength_Y,0,0,0,0, workSurface).setJointVelocityRel(0.1));
		handDrill.move(linRel(-workSurfaceLength_X,0,0,0,0,0, workSurface).setJointVelocityRel(0.1));
		handDrill.move(linRel(0,-workSurfaceLength_Y,0,0,0,0, workSurface).setJointVelocityRel(0.1));
		handDrill.move(linRel(0,0,100,0,0,0, workSurface).setJointVelocityRel(0.1));
		
		handDrill.move(ptp(getApplicationData().getFrame("/handDrillHome")).setJointVelocityRel(0.1));
	}
	
}
