﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using CapToolBox;
using System.IO;
using Phidget22;
using Bluebit.MatrixLibrary;


namespace Kuka_HandDrillTestStand
{
  public partial class HandDrillTestStand_GUI : Form
  {
    bool programRunning = true;

    //time related variable
    Stopwatch recordedTime_sw = new Stopwatch();

    //robot related variables
    BoostSoc robotSoc = new BoostSoc(false, 30);
    string robotDataIn = "";
    bool recordRobotData = false;
    string[] robotDataHeaders = new string[9];
    string[] drillDataHeaders = new string[2];
    string[] thermocoupleDataHeaders = new string[4];
    static TemperatureSensor[] thermocouple = new TemperatureSensor[4];

    bool recordingData = false;
    List<string> recordedData = new List<string>();

    string FileNameDateTime;

    bool thermocoupleConnected = false;

    public HandDrillTestStand_GUI()
    {
      InitializeComponent();
      
      //set button states
      startRecordData_btn.Visible = true;
      stopRecordData_btn.Visible = false;
      stopRecordData_btn.Location = startRecordData_btn.Location;

      //connect to robot as a boost soc server
      robotSoc.serverConnect(2000, false, true);
      Thread threadReadIn = new Thread(() => readInMessages(robotSoc, ref robotDataIn));
      threadReadIn.Start();
      updateData_tmr.Enabled = true;

      //setup robot column names
      robotDataHeaders[0] = "Pos X (mm)";
      robotDataHeaders[1] = "Pos Y (mm)";
      robotDataHeaders[2] = "Pos Z (mm)";
      robotDataHeaders[3] = "Pos W (rad)";
      robotDataHeaders[4] = "Pos P (rad)";
      robotDataHeaders[5] = "Pos R (rad)";
      robotDataHeaders[6] = "Force X (N)";
      robotDataHeaders[7] = "Force Y (N)";
      robotDataHeaders[8] = "Force Z (N)";

      //thermocouple connect
      thermocoupleSetup();

      //thermocouple data headers
      thermocoupleDataHeaders[0] = "Temp A (°C)";
      thermocoupleDataHeaders[1] = "Temp B (°C)";
      thermocoupleDataHeaders[2] = "Temp C (°C)";
      thermocoupleDataHeaders[3] = "Temp D (°C)";

      //drill data headers
      drillDataHeaders[0] = "Speed (rot/min)";
      drillDataHeaders[1] = "Current (A)";
    }

    private void startRecordData_btn_Click(object sender, EventArgs e)
    {
      recordingData = true;
      startRecordData_btn.Visible = false;
      stopRecordData_btn.Visible = true;
      export_btn.Enabled = false;
      FileNameDateTime = DateTime.Now.ToString("MM-dd-yyyy(HH.mm.ss)");      //stores dateTime in string
      setupDataHeaders();
      recordedTime_sw.Reset();
      recordedTime_sw.Start();

      export_btn.Text = "Export to CSV";
    }

    private void stopRecordData_btn_Click(object sender, EventArgs e)
    {
      recordingData = false;
      startRecordData_btn.Visible = true;
      stopRecordData_btn.Visible = false;
      export_btn.Enabled = true;
      recordedTime_sw.Stop();
    }


    //read an incoming message with boost soc
    private void readInMessages(BoostSoc connectedSoc, ref string receivedData)
    {
      bool conDroppedFlag = false;
      while (programRunning)
      {
        Thread.Sleep(25);
        string inMessage = connectedSoc.getLatestMessage();
        if (inMessage != "")
        {
          receivedData = "";
          receivedData = inMessage;
        }
        if (!connectedSoc.getConnected() && !conDroppedFlag)
        {
          conDroppedFlag = true;
        }
        else if (connectedSoc.getConnected() && conDroppedFlag)
        {
          conDroppedFlag = false;
        }
      }
    }
    
    //parse a string into a list of smaller strings between commas
    public List<string> stringCommaParse(string toParse)
    {
      string tempParse = "";
      List<string> parsedStrings = new List<string>();
      
      for(int i = 0; i < toParse.Length; i++)
      {
        if (toParse[i] != ',')
        {
          tempParse += toParse[i];
        }
        if (toParse[i] == ',' || i == toParse.Length -1 )
        {
          parsedStrings.Add(tempParse);
          tempParse = "";
        }
      }
      return parsedStrings;
    }


    public void updateRobotInfo()
    {
      List<string> robotDataParsed = stringCommaParse(robotDataIn);
      if(robotDataParsed.Count == 9)
      {
        xPos_lbl.Text = "X: " + robotDataParsed[0];
        yPos_lbl.Text = "Y: " + robotDataParsed[1];
        zPos_lbl.Text = "Z: " + robotDataParsed[2];
        wPos_lbl.Text = "A: " + robotDataParsed[3];
        pPos_lbl.Text = "B: " + robotDataParsed[4];
        rPos_lbl.Text = "C: " + robotDataParsed[5];
        xForce_lbl.Text = "X: " + robotDataParsed[6];
        yForce_lbl.Text = "Y: " + robotDataParsed[7];
        zForce_lbl.Text = "Z: " + robotDataParsed[8];
      }
      if (robotSoc.getConnected())
      {
        robotConnect_lbl.Text = "Connected";
        robotConnect_lbl.BackColor = Color.SeaGreen;
      }
      else
      {
        robotConnect_lbl.Text = "Not Connected";
        robotConnect_lbl.BackColor = Color.Khaki;
      }
    }

    public void updateThermocoupleInfo()
    {
      
      thermocoupleConnected = false;
      try
      {
        if (thermocouple[0] != null && thermocouple[0].Temperature < 200)
        {
          thermocoupleA_lbl.Text = "A: " + thermocouple[0].Temperature.ToString();
          thermocoupleConnected = true;
        }
        else
        {
          thermocoupleA_lbl.Text = "A: ";
        }
        if (thermocouple[1] != null && thermocouple[1].Temperature < 200)
        {
          thermocoupleB_lbl.Text = "B: " + thermocouple[1].Temperature.ToString();
          thermocoupleConnected = true;
        }
        else
        {
          thermocoupleB_lbl.Text = "B: ";
        }
        if (thermocouple[2] != null && thermocouple[2].Temperature < 200)
        {
          thermocoupleC_lbl.Text = "C: " + thermocouple[2].Temperature.ToString();
          thermocoupleConnected = true;
        }
        else
        {
          thermocoupleC_lbl.Text = "C: ";
        }
        if (thermocouple[3] != null && thermocouple[3].Temperature < 200)
        {
          thermocoupleD_lbl.Text = "D: " + thermocouple[3].Temperature.ToString();
          thermocoupleConnected = true;
        }
        else
        {
          thermocoupleD_lbl.Text = "D: ";
        }
      }
      catch
      {
        //thermocoupleSetup();
      }


      if (thermocoupleConnected)
      {
        thermocoupleConnect_lbl.Text = "Connected";
        thermocoupleConnect_lbl.BackColor = Color.SeaGreen;
      }
      else
      {
        thermocoupleConnect_lbl.Text = "Not Connected";
        thermocoupleConnect_lbl.BackColor = Color.Khaki;
      }
    }


    private void updateData_tmr_Tick(object sender, EventArgs e)
    {
      elapsedTime_lbl.Text = (recordedTime_sw.Elapsed.TotalMilliseconds / 1000.0).ToString();
      updateRobotInfo();
      updateThermocoupleInfo();


      if (recordingData)
      {
        addNewData();
      }
    }


    private void setupDataHeaders()
    {
      recordedData = new List<string>();
      string headers;
      headers = "Time (sec), ";

      if (robotSoc.getConnected())
      {
        recordRobotData = true;
        foreach(string header in robotDataHeaders) 
        {
          headers += header + ", ";
        }
      }
      recordedData.Add(headers);
    }


    private void addNewData()
    {
      string freshData = (recordedTime_sw.Elapsed.TotalMilliseconds / 1000.0) + ", ";

      if (recordRobotData)
      {
        freshData += robotDataIn;
      }
      recordedData.Add(freshData);
    }


    //Export the Data to .csv
    private void export_btn_Click(object sender, EventArgs e)
    {
      try
      {
        File.WriteAllLines(txtFileLocation.Text + txtFileName.Text + " " + FileNameDateTime + ".csv", recordedData);
        export_btn.Text = "Export Successful";
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.Message, "Write to CSV Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }


    private static void TemperatureSensor_TemperatureChange(object sender, Phidget22.Events.TemperatureSensorTemperatureChangeEventArgs e)
    {
      Phidget22.TemperatureSensor evChannel = (Phidget22.TemperatureSensor)sender;
    }

    void thermocoupleSetup()
    {
      for(int i = 0; i < 4; i++)
      {
        thermocoupleConnected = false;
        thermocouple[i] = new TemperatureSensor();

        //Set addressing parameters to specify which channel to open (if any)
        thermocouple[i].Channel = i;

        //Assign any event handlers you need before calling open so that no events are missed.
        thermocouple[i].TemperatureChange += TemperatureSensor_TemperatureChange;

        //Open your Phidgets and wait for attachment
        try
        {
          thermocouple[i].Open(1000);
          thermocoupleConnected = true;
        }
        catch
        {
          thermocoupleConnected = false;
        }
      }

    }


    private void HandDrillTestStand_GUI_FormClosed(object sender, FormClosedEventArgs e)
    {
      try
      {
        for (int i = 0; i < 4; i++)
        {
          thermocouple[i].Close();
        }
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.Message, "Thermocouple Close Failure", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }

      try
      {
        robotSoc.killBoostSoc();
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.Message, "Boost Soc Kill Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }




    public Matrix forceMatrix_1x4(double x, double y, double z)
    {
      Matrix forceMatrix = new Matrix(1, 4);
      double[] forceVals = new double[] { x, y, z, 0 };
      forceMatrix.SetRow(0, forceVals);
      return forceMatrix;
    }

    public Matrix rotationMatrix_4x4(double x, double y, double z, double zAlpha, double yBeta, double xGamma)
    {
      double A = Math.Cos(xGamma);
      double B = Math.Sin(xGamma);
      double C = Math.Cos(yBeta);
      double D = Math.Sin(yBeta);
      double E = Math.Cos(zAlpha);
      double F = Math.Sin(zAlpha);

      double[] colVals_0 = new double[] { C * E, C * F, -D, 0 };
      double[] colVals_1 = new double[] { B * E * D - A * F, B * F * D + A * E, B * C, 0 };
      double[] colVals_2 = new double[] { A * E * D + B * F, A * F * D - B * E, A * C, 0 };
      double[] colVals_3 = new double[] { x, y, z, 1 };

      Matrix rotationMatrix = new Matrix(4, 4);

      rotationMatrix.SetColumn(0, colVals_0);
      rotationMatrix.SetColumn(1, colVals_1);
      rotationMatrix.SetColumn(2, colVals_2);
      rotationMatrix.SetColumn(3, colVals_3);

      return rotationMatrix;
    }


  }


}