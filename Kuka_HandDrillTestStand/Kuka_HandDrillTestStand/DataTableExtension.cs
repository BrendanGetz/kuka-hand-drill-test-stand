﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//This class is for exporting a DataTable to a .csv file.  
//This the Write to csv function requires the input of a data table and a string file path. 
//used to export all of the data we have collected from the DAQ to a usable excel file.
public static class DataTableCsvExtensions
{
  public static void WriteToCsvFile(this DataTable dataTable, string filePath)
  {
    StringBuilder fileContent = new StringBuilder();
    foreach (var col in dataTable.Columns)
    {
      fileContent.Append(col.ToString() + ",");
    }
    fileContent.Replace(",", System.Environment.NewLine, fileContent.Length - 1, 1);
    foreach (DataRow dr in dataTable.Rows)
    {
      foreach (var column in dr.ItemArray)
      {
        fileContent.Append("\"" + column.ToString() + "\",");
      }
      fileContent.Replace(",", System.Environment.NewLine, fileContent.Length - 1, 1);
    }
    System.IO.File.WriteAllText(filePath, fileContent.ToString());
  }
}
