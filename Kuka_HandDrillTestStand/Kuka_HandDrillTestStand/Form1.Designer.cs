﻿namespace Kuka_HandDrillTestStand
{
  partial class HandDrillTestStand_GUI
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

        
    
    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HandDrillTestStand_GUI));
      this.robotPosTitle_lbl = new System.Windows.Forms.Label();
      this.xPos_lbl = new System.Windows.Forms.Label();
      this.yPos_lbl = new System.Windows.Forms.Label();
      this.zPos_lbl = new System.Windows.Forms.Label();
      this.zForce_lbl = new System.Windows.Forms.Label();
      this.yForce_lbl = new System.Windows.Forms.Label();
      this.xForce_lbl = new System.Windows.Forms.Label();
      this.robotForceTitle_lbl = new System.Windows.Forms.Label();
      this.thermocoupleC_lbl = new System.Windows.Forms.Label();
      this.thermocoupleB_lbl = new System.Windows.Forms.Label();
      this.thermocoupleA_lbl = new System.Windows.Forms.Label();
      this.thermocoupleTitle_lbl = new System.Windows.Forms.Label();
      this.drillCurrent_lbl = new System.Windows.Forms.Label();
      this.drillSpeed_lbl = new System.Windows.Forms.Label();
      this.drillController_lbl = new System.Windows.Forms.Label();
      this.updateData_tmr = new System.Windows.Forms.Timer(this.components);
      this.robotConnect_lbl = new System.Windows.Forms.Label();
      this.robot_pnl = new System.Windows.Forms.Panel();
      this.wPos_lbl = new System.Windows.Forms.Label();
      this.pPos_lbl = new System.Windows.Forms.Label();
      this.rPos_lbl = new System.Windows.Forms.Label();
      this.robot_picbox = new System.Windows.Forms.PictureBox();
      this.thermocouple_pnl = new System.Windows.Forms.Panel();
      this.thermocoupleConnect_lbl = new System.Windows.Forms.Label();
      this.thermocouple_picbox = new System.Windows.Forms.PictureBox();
      this.txtFileLocation = new System.Windows.Forms.TextBox();
      this.lblCSV = new System.Windows.Forms.Label();
      this.txtFileName = new System.Windows.Forms.TextBox();
      this.lblFileName = new System.Windows.Forms.Label();
      this.lblFileLocation = new System.Windows.Forms.Label();
      this.dataSet1 = new System.Data.DataSet();
      this.elapsedTime_lbl = new System.Windows.Forms.Label();
      this.time_pnl = new System.Windows.Forms.Panel();
      this.pictureBox1 = new System.Windows.Forms.PictureBox();
      this.timeTitle_lbl = new System.Windows.Forms.Label();
      this.export_btn = new System.Windows.Forms.Button();
      this.stopRecordData_btn = new System.Windows.Forms.Button();
      this.startRecordData_btn = new System.Windows.Forms.Button();
      this.drill_pnl = new System.Windows.Forms.Panel();
      this.label1 = new System.Windows.Forms.Label();
      this.pictureBox2 = new System.Windows.Forms.PictureBox();
      this.thermocoupleD_lbl = new System.Windows.Forms.Label();
      this.robot_pnl.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.robot_picbox)).BeginInit();
      this.thermocouple_pnl.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.thermocouple_picbox)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
      this.time_pnl.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
      this.drill_pnl.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
      this.SuspendLayout();
      // 
      // robotPosTitle_lbl
      // 
      this.robotPosTitle_lbl.AutoSize = true;
      this.robotPosTitle_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.robotPosTitle_lbl.Location = new System.Drawing.Point(193, 13);
      this.robotPosTitle_lbl.Name = "robotPosTitle_lbl";
      this.robotPosTitle_lbl.Size = new System.Drawing.Size(182, 20);
      this.robotPosTitle_lbl.TabIndex = 0;
      this.robotPosTitle_lbl.Text = "Robot Position (mm)";
      // 
      // xPos_lbl
      // 
      this.xPos_lbl.AutoSize = true;
      this.xPos_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.xPos_lbl.Location = new System.Drawing.Point(193, 42);
      this.xPos_lbl.Name = "xPos_lbl";
      this.xPos_lbl.Size = new System.Drawing.Size(25, 20);
      this.xPos_lbl.TabIndex = 1;
      this.xPos_lbl.Text = "X:";
      // 
      // yPos_lbl
      // 
      this.yPos_lbl.AutoSize = true;
      this.yPos_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.yPos_lbl.Location = new System.Drawing.Point(193, 62);
      this.yPos_lbl.Name = "yPos_lbl";
      this.yPos_lbl.Size = new System.Drawing.Size(24, 20);
      this.yPos_lbl.TabIndex = 2;
      this.yPos_lbl.Text = "Y:";
      // 
      // zPos_lbl
      // 
      this.zPos_lbl.AutoSize = true;
      this.zPos_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.zPos_lbl.Location = new System.Drawing.Point(193, 82);
      this.zPos_lbl.Name = "zPos_lbl";
      this.zPos_lbl.Size = new System.Drawing.Size(23, 20);
      this.zPos_lbl.TabIndex = 3;
      this.zPos_lbl.Text = "Z:";
      // 
      // zForce_lbl
      // 
      this.zForce_lbl.AutoSize = true;
      this.zForce_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.zForce_lbl.Location = new System.Drawing.Point(193, 240);
      this.zForce_lbl.Name = "zForce_lbl";
      this.zForce_lbl.Size = new System.Drawing.Size(23, 20);
      this.zForce_lbl.TabIndex = 7;
      this.zForce_lbl.Text = "Z:";
      // 
      // yForce_lbl
      // 
      this.yForce_lbl.AutoSize = true;
      this.yForce_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.yForce_lbl.Location = new System.Drawing.Point(193, 220);
      this.yForce_lbl.Name = "yForce_lbl";
      this.yForce_lbl.Size = new System.Drawing.Size(24, 20);
      this.yForce_lbl.TabIndex = 6;
      this.yForce_lbl.Text = "Y:";
      // 
      // xForce_lbl
      // 
      this.xForce_lbl.AutoSize = true;
      this.xForce_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.xForce_lbl.Location = new System.Drawing.Point(193, 200);
      this.xForce_lbl.Name = "xForce_lbl";
      this.xForce_lbl.Size = new System.Drawing.Size(25, 20);
      this.xForce_lbl.TabIndex = 5;
      this.xForce_lbl.Text = "X:";
      // 
      // robotForceTitle_lbl
      // 
      this.robotForceTitle_lbl.AutoSize = true;
      this.robotForceTitle_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.robotForceTitle_lbl.Location = new System.Drawing.Point(193, 171);
      this.robotForceTitle_lbl.Name = "robotForceTitle_lbl";
      this.robotForceTitle_lbl.Size = new System.Drawing.Size(145, 20);
      this.robotForceTitle_lbl.TabIndex = 4;
      this.robotForceTitle_lbl.Text = "Robot Force (N)";
      // 
      // thermocoupleC_lbl
      // 
      this.thermocoupleC_lbl.AutoSize = true;
      this.thermocoupleC_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.thermocoupleC_lbl.Location = new System.Drawing.Point(193, 103);
      this.thermocoupleC_lbl.Name = "thermocoupleC_lbl";
      this.thermocoupleC_lbl.Size = new System.Drawing.Size(26, 20);
      this.thermocoupleC_lbl.TabIndex = 12;
      this.thermocoupleC_lbl.Text = "C:";
      // 
      // thermocoupleB_lbl
      // 
      this.thermocoupleB_lbl.AutoSize = true;
      this.thermocoupleB_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.thermocoupleB_lbl.Location = new System.Drawing.Point(193, 83);
      this.thermocoupleB_lbl.Name = "thermocoupleB_lbl";
      this.thermocoupleB_lbl.Size = new System.Drawing.Size(26, 20);
      this.thermocoupleB_lbl.TabIndex = 11;
      this.thermocoupleB_lbl.Text = "B:";
      // 
      // thermocoupleA_lbl
      // 
      this.thermocoupleA_lbl.AutoSize = true;
      this.thermocoupleA_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.thermocoupleA_lbl.Location = new System.Drawing.Point(194, 63);
      this.thermocoupleA_lbl.Name = "thermocoupleA_lbl";
      this.thermocoupleA_lbl.Size = new System.Drawing.Size(25, 20);
      this.thermocoupleA_lbl.TabIndex = 10;
      this.thermocoupleA_lbl.Text = "A:";
      // 
      // thermocoupleTitle_lbl
      // 
      this.thermocoupleTitle_lbl.AutoSize = true;
      this.thermocoupleTitle_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.thermocoupleTitle_lbl.Location = new System.Drawing.Point(194, 34);
      this.thermocoupleTitle_lbl.Name = "thermocoupleTitle_lbl";
      this.thermocoupleTitle_lbl.Size = new System.Drawing.Size(168, 20);
      this.thermocoupleTitle_lbl.TabIndex = 9;
      this.thermocoupleTitle_lbl.Text = "Thermocouple (°C)";
      // 
      // drillCurrent_lbl
      // 
      this.drillCurrent_lbl.AutoSize = true;
      this.drillCurrent_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.drillCurrent_lbl.Location = new System.Drawing.Point(194, 113);
      this.drillCurrent_lbl.Name = "drillCurrent_lbl";
      this.drillCurrent_lbl.Size = new System.Drawing.Size(70, 20);
      this.drillCurrent_lbl.TabIndex = 15;
      this.drillCurrent_lbl.Text = "Current:";
      // 
      // drillSpeed_lbl
      // 
      this.drillSpeed_lbl.AutoSize = true;
      this.drillSpeed_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.drillSpeed_lbl.Location = new System.Drawing.Point(194, 93);
      this.drillSpeed_lbl.Name = "drillSpeed_lbl";
      this.drillSpeed_lbl.Size = new System.Drawing.Size(61, 20);
      this.drillSpeed_lbl.TabIndex = 14;
      this.drillSpeed_lbl.Text = "Speed:";
      // 
      // drillController_lbl
      // 
      this.drillController_lbl.AutoSize = true;
      this.drillController_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.drillController_lbl.Location = new System.Drawing.Point(194, 64);
      this.drillController_lbl.Name = "drillController_lbl";
      this.drillController_lbl.Size = new System.Drawing.Size(134, 20);
      this.drillController_lbl.TabIndex = 13;
      this.drillController_lbl.Text = "Drill Controller";
      // 
      // updateData_tmr
      // 
      this.updateData_tmr.Interval = 35;
      this.updateData_tmr.Tick += new System.EventHandler(this.updateData_tmr_Tick);
      // 
      // robotConnect_lbl
      // 
      this.robotConnect_lbl.AutoSize = true;
      this.robotConnect_lbl.BackColor = System.Drawing.Color.Khaki;
      this.robotConnect_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.robotConnect_lbl.Location = new System.Drawing.Point(100, 282);
      this.robotConnect_lbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.robotConnect_lbl.Name = "robotConnect_lbl";
      this.robotConnect_lbl.Size = new System.Drawing.Size(209, 31);
      this.robotConnect_lbl.TabIndex = 19;
      this.robotConnect_lbl.Text = "Not Connected";
      // 
      // robot_pnl
      // 
      this.robot_pnl.BackColor = System.Drawing.SystemColors.Control;
      this.robot_pnl.Controls.Add(this.wPos_lbl);
      this.robot_pnl.Controls.Add(this.pPos_lbl);
      this.robot_pnl.Controls.Add(this.rPos_lbl);
      this.robot_pnl.Controls.Add(this.robot_picbox);
      this.robot_pnl.Controls.Add(this.robotPosTitle_lbl);
      this.robot_pnl.Controls.Add(this.robotConnect_lbl);
      this.robot_pnl.Controls.Add(this.xPos_lbl);
      this.robot_pnl.Controls.Add(this.yPos_lbl);
      this.robot_pnl.Controls.Add(this.zPos_lbl);
      this.robot_pnl.Controls.Add(this.robotForceTitle_lbl);
      this.robot_pnl.Controls.Add(this.xForce_lbl);
      this.robot_pnl.Controls.Add(this.yForce_lbl);
      this.robot_pnl.Controls.Add(this.zForce_lbl);
      this.robot_pnl.Location = new System.Drawing.Point(106, 339);
      this.robot_pnl.Name = "robot_pnl";
      this.robot_pnl.Size = new System.Drawing.Size(408, 341);
      this.robot_pnl.TabIndex = 21;
      // 
      // wPos_lbl
      // 
      this.wPos_lbl.AutoSize = true;
      this.wPos_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.wPos_lbl.Location = new System.Drawing.Point(191, 102);
      this.wPos_lbl.Name = "wPos_lbl";
      this.wPos_lbl.Size = new System.Drawing.Size(30, 20);
      this.wPos_lbl.TabIndex = 21;
      this.wPos_lbl.Text = "W:";
      // 
      // pPos_lbl
      // 
      this.pPos_lbl.AutoSize = true;
      this.pPos_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.pPos_lbl.Location = new System.Drawing.Point(191, 122);
      this.pPos_lbl.Name = "pPos_lbl";
      this.pPos_lbl.Size = new System.Drawing.Size(25, 20);
      this.pPos_lbl.TabIndex = 22;
      this.pPos_lbl.Text = "P:";
      // 
      // rPos_lbl
      // 
      this.rPos_lbl.AutoSize = true;
      this.rPos_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.rPos_lbl.Location = new System.Drawing.Point(191, 142);
      this.rPos_lbl.Name = "rPos_lbl";
      this.rPos_lbl.Size = new System.Drawing.Size(26, 20);
      this.rPos_lbl.TabIndex = 23;
      this.rPos_lbl.Text = "R:";
      // 
      // robot_picbox
      // 
      this.robot_picbox.Image = ((System.Drawing.Image)(resources.GetObject("robot_picbox.Image")));
      this.robot_picbox.Location = new System.Drawing.Point(46, 91);
      this.robot_picbox.Name = "robot_picbox";
      this.robot_picbox.Size = new System.Drawing.Size(112, 100);
      this.robot_picbox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
      this.robot_picbox.TabIndex = 20;
      this.robot_picbox.TabStop = false;
      // 
      // thermocouple_pnl
      // 
      this.thermocouple_pnl.BackColor = System.Drawing.SystemColors.Control;
      this.thermocouple_pnl.Controls.Add(this.thermocoupleD_lbl);
      this.thermocouple_pnl.Controls.Add(this.thermocoupleConnect_lbl);
      this.thermocouple_pnl.Controls.Add(this.thermocouple_picbox);
      this.thermocouple_pnl.Controls.Add(this.thermocoupleTitle_lbl);
      this.thermocouple_pnl.Controls.Add(this.thermocoupleA_lbl);
      this.thermocouple_pnl.Controls.Add(this.thermocoupleB_lbl);
      this.thermocouple_pnl.Controls.Add(this.thermocoupleC_lbl);
      this.thermocouple_pnl.Location = new System.Drawing.Point(557, 161);
      this.thermocouple_pnl.Name = "thermocouple_pnl";
      this.thermocouple_pnl.Size = new System.Drawing.Size(408, 225);
      this.thermocouple_pnl.TabIndex = 23;
      // 
      // thermocoupleConnect_lbl
      // 
      this.thermocoupleConnect_lbl.AutoSize = true;
      this.thermocoupleConnect_lbl.BackColor = System.Drawing.Color.Khaki;
      this.thermocoupleConnect_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.thermocoupleConnect_lbl.Location = new System.Drawing.Point(87, 165);
      this.thermocoupleConnect_lbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.thermocoupleConnect_lbl.Name = "thermocoupleConnect_lbl";
      this.thermocoupleConnect_lbl.Size = new System.Drawing.Size(209, 31);
      this.thermocoupleConnect_lbl.TabIndex = 23;
      this.thermocoupleConnect_lbl.Text = "Not Connected";
      // 
      // thermocouple_picbox
      // 
      this.thermocouple_picbox.Image = ((System.Drawing.Image)(resources.GetObject("thermocouple_picbox.Image")));
      this.thermocouple_picbox.Location = new System.Drawing.Point(42, 34);
      this.thermocouple_picbox.Name = "thermocouple_picbox";
      this.thermocouple_picbox.Size = new System.Drawing.Size(112, 100);
      this.thermocouple_picbox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
      this.thermocouple_picbox.TabIndex = 22;
      this.thermocouple_picbox.TabStop = false;
      // 
      // txtFileLocation
      // 
      this.txtFileLocation.Location = new System.Drawing.Point(635, 42);
      this.txtFileLocation.Name = "txtFileLocation";
      this.txtFileLocation.Size = new System.Drawing.Size(402, 22);
      this.txtFileLocation.TabIndex = 229;
      this.txtFileLocation.Text = "C:\\Users\\bdg\\Desktop\\Drill Robot Tests\\Robot Data\\";
      // 
      // lblCSV
      // 
      this.lblCSV.AutoSize = true;
      this.lblCSV.Location = new System.Drawing.Point(919, 77);
      this.lblCSV.Name = "lblCSV";
      this.lblCSV.Size = new System.Drawing.Size(134, 17);
      this.lblCSV.TabIndex = 228;
      this.lblCSV.Text = "+ DateTime +.csv";
      // 
      // txtFileName
      // 
      this.txtFileName.Location = new System.Drawing.Point(635, 74);
      this.txtFileName.Name = "txtFileName";
      this.txtFileName.Size = new System.Drawing.Size(278, 22);
      this.txtFileName.TabIndex = 227;
      this.txtFileName.Text = "Drill Number";
      // 
      // lblFileName
      // 
      this.lblFileName.AutoSize = true;
      this.lblFileName.Location = new System.Drawing.Point(554, 77);
      this.lblFileName.Name = "lblFileName";
      this.lblFileName.Size = new System.Drawing.Size(85, 17);
      this.lblFileName.TabIndex = 226;
      this.lblFileName.Text = "File Name:";
      // 
      // lblFileLocation
      // 
      this.lblFileLocation.AutoSize = true;
      this.lblFileLocation.Location = new System.Drawing.Point(533, 45);
      this.lblFileLocation.Name = "lblFileLocation";
      this.lblFileLocation.Size = new System.Drawing.Size(106, 17);
      this.lblFileLocation.TabIndex = 225;
      this.lblFileLocation.Text = "File Location:";
      // 
      // dataSet1
      // 
      this.dataSet1.DataSetName = "NewDataSet";
      // 
      // elapsedTime_lbl
      // 
      this.elapsedTime_lbl.AutoSize = true;
      this.elapsedTime_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.elapsedTime_lbl.Location = new System.Drawing.Point(193, 73);
      this.elapsedTime_lbl.Name = "elapsedTime_lbl";
      this.elapsedTime_lbl.Size = new System.Drawing.Size(31, 20);
      this.elapsedTime_lbl.TabIndex = 24;
      this.elapsedTime_lbl.Text = "0.0";
      // 
      // time_pnl
      // 
      this.time_pnl.BackColor = System.Drawing.SystemColors.Control;
      this.time_pnl.Controls.Add(this.elapsedTime_lbl);
      this.time_pnl.Controls.Add(this.pictureBox1);
      this.time_pnl.Controls.Add(this.timeTitle_lbl);
      this.time_pnl.Location = new System.Drawing.Point(106, 161);
      this.time_pnl.Name = "time_pnl";
      this.time_pnl.Size = new System.Drawing.Size(408, 149);
      this.time_pnl.TabIndex = 231;
      // 
      // pictureBox1
      // 
      this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
      this.pictureBox1.Location = new System.Drawing.Point(46, 20);
      this.pictureBox1.Name = "pictureBox1";
      this.pictureBox1.Size = new System.Drawing.Size(112, 100);
      this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
      this.pictureBox1.TabIndex = 22;
      this.pictureBox1.TabStop = false;
      // 
      // timeTitle_lbl
      // 
      this.timeTitle_lbl.AutoSize = true;
      this.timeTitle_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.timeTitle_lbl.Location = new System.Drawing.Point(193, 44);
      this.timeTitle_lbl.Name = "timeTitle_lbl";
      this.timeTitle_lbl.Size = new System.Drawing.Size(186, 20);
      this.timeTitle_lbl.TabIndex = 9;
      this.timeTitle_lbl.Text = "Recorded Time (sec)";
      // 
      // export_btn
      // 
      this.export_btn.FlatAppearance.BorderSize = 3;
      this.export_btn.Image = ((System.Drawing.Image)(resources.GetObject("export_btn.Image")));
      this.export_btn.Location = new System.Drawing.Point(348, 12);
      this.export_btn.Name = "export_btn";
      this.export_btn.Size = new System.Drawing.Size(144, 120);
      this.export_btn.TabIndex = 230;
      this.export_btn.Text = "Export to CSV";
      this.export_btn.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
      this.export_btn.UseVisualStyleBackColor = true;
      this.export_btn.Click += new System.EventHandler(this.export_btn_Click);
      // 
      // stopRecordData_btn
      // 
      this.stopRecordData_btn.FlatAppearance.BorderSize = 3;
      this.stopRecordData_btn.Image = ((System.Drawing.Image)(resources.GetObject("stopRecordData_btn.Image")));
      this.stopRecordData_btn.Location = new System.Drawing.Point(165, 32);
      this.stopRecordData_btn.Name = "stopRecordData_btn";
      this.stopRecordData_btn.Size = new System.Drawing.Size(144, 120);
      this.stopRecordData_btn.TabIndex = 17;
      this.stopRecordData_btn.Text = "Stop Recording";
      this.stopRecordData_btn.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
      this.stopRecordData_btn.UseVisualStyleBackColor = true;
      this.stopRecordData_btn.Click += new System.EventHandler(this.stopRecordData_btn_Click);
      // 
      // startRecordData_btn
      // 
      this.startRecordData_btn.FlatAppearance.BorderSize = 3;
      this.startRecordData_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.startRecordData_btn.Image = ((System.Drawing.Image)(resources.GetObject("startRecordData_btn.Image")));
      this.startRecordData_btn.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
      this.startRecordData_btn.Location = new System.Drawing.Point(138, 12);
      this.startRecordData_btn.Name = "startRecordData_btn";
      this.startRecordData_btn.Size = new System.Drawing.Size(144, 120);
      this.startRecordData_btn.TabIndex = 16;
      this.startRecordData_btn.Text = "Record Data";
      this.startRecordData_btn.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
      this.startRecordData_btn.UseVisualStyleBackColor = true;
      this.startRecordData_btn.Click += new System.EventHandler(this.startRecordData_btn_Click);
      // 
      // drill_pnl
      // 
      this.drill_pnl.BackColor = System.Drawing.SystemColors.Control;
      this.drill_pnl.Controls.Add(this.label1);
      this.drill_pnl.Controls.Add(this.pictureBox2);
      this.drill_pnl.Controls.Add(this.drillController_lbl);
      this.drill_pnl.Controls.Add(this.drillSpeed_lbl);
      this.drill_pnl.Controls.Add(this.drillCurrent_lbl);
      this.drill_pnl.Location = new System.Drawing.Point(557, 421);
      this.drill_pnl.Name = "drill_pnl";
      this.drill_pnl.Size = new System.Drawing.Size(408, 259);
      this.drill_pnl.TabIndex = 24;
      this.drill_pnl.Visible = false;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.BackColor = System.Drawing.Color.Khaki;
      this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label1.Location = new System.Drawing.Point(96, 197);
      this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(209, 31);
      this.label1.TabIndex = 23;
      this.label1.Text = "Not Connected";
      // 
      // pictureBox2
      // 
      this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
      this.pictureBox2.Location = new System.Drawing.Point(42, 64);
      this.pictureBox2.Name = "pictureBox2";
      this.pictureBox2.Size = new System.Drawing.Size(112, 100);
      this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
      this.pictureBox2.TabIndex = 22;
      this.pictureBox2.TabStop = false;
      // 
      // thermocoupleD_lbl
      // 
      this.thermocoupleD_lbl.AutoSize = true;
      this.thermocoupleD_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.thermocoupleD_lbl.Location = new System.Drawing.Point(192, 123);
      this.thermocoupleD_lbl.Name = "thermocoupleD_lbl";
      this.thermocoupleD_lbl.Size = new System.Drawing.Size(27, 20);
      this.thermocoupleD_lbl.TabIndex = 24;
      this.thermocoupleD_lbl.Text = "D:";
      // 
      // HandDrillTestStand_GUI
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 16F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.PowderBlue;
      this.ClientSize = new System.Drawing.Size(1883, 869);
      this.Controls.Add(this.drill_pnl);
      this.Controls.Add(this.time_pnl);
      this.Controls.Add(this.export_btn);
      this.Controls.Add(this.txtFileLocation);
      this.Controls.Add(this.lblCSV);
      this.Controls.Add(this.txtFileName);
      this.Controls.Add(this.lblFileName);
      this.Controls.Add(this.lblFileLocation);
      this.Controls.Add(this.thermocouple_pnl);
      this.Controls.Add(this.robot_pnl);
      this.Controls.Add(this.stopRecordData_btn);
      this.Controls.Add(this.startRecordData_btn);
      this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.Name = "HandDrillTestStand_GUI";
      this.Text = "Hand Drill Test Stand_GUI";
      this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.HandDrillTestStand_GUI_FormClosed);
      this.robot_pnl.ResumeLayout(false);
      this.robot_pnl.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.robot_picbox)).EndInit();
      this.thermocouple_pnl.ResumeLayout(false);
      this.thermocouple_pnl.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.thermocouple_picbox)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
      this.time_pnl.ResumeLayout(false);
      this.time_pnl.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
      this.drill_pnl.ResumeLayout(false);
      this.drill_pnl.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Label robotPosTitle_lbl;
    private System.Windows.Forms.Label xPos_lbl;
    private System.Windows.Forms.Label yPos_lbl;
    private System.Windows.Forms.Label zPos_lbl;
    private System.Windows.Forms.Label zForce_lbl;
    private System.Windows.Forms.Label yForce_lbl;
    private System.Windows.Forms.Label xForce_lbl;
    private System.Windows.Forms.Label robotForceTitle_lbl;
    private System.Windows.Forms.Label thermocoupleC_lbl;
    private System.Windows.Forms.Label thermocoupleB_lbl;
    private System.Windows.Forms.Label thermocoupleA_lbl;
    private System.Windows.Forms.Label thermocoupleTitle_lbl;
    private System.Windows.Forms.Label drillCurrent_lbl;
    private System.Windows.Forms.Label drillSpeed_lbl;
    private System.Windows.Forms.Label drillController_lbl;
    private System.Windows.Forms.Button startRecordData_btn;
    private System.Windows.Forms.Button stopRecordData_btn;
    private System.Windows.Forms.Timer updateData_tmr;
    private System.Windows.Forms.Label robotConnect_lbl;
    private System.Windows.Forms.PictureBox robot_picbox;
    private System.Windows.Forms.Panel robot_pnl;
    private System.Windows.Forms.PictureBox thermocouple_picbox;
    private System.Windows.Forms.Panel thermocouple_pnl;
    private System.Windows.Forms.Label thermocoupleConnect_lbl;
    private System.Windows.Forms.TextBox txtFileLocation;
    private System.Windows.Forms.Label lblCSV;
    private System.Windows.Forms.TextBox txtFileName;
    private System.Windows.Forms.Label lblFileName;
    private System.Windows.Forms.Label lblFileLocation;
    private System.Windows.Forms.Button export_btn;
    private System.Windows.Forms.Label wPos_lbl;
    private System.Windows.Forms.Label pPos_lbl;
    private System.Windows.Forms.Label rPos_lbl;
    private System.Data.DataSet dataSet1;
    private System.Windows.Forms.Label elapsedTime_lbl;
    private System.Windows.Forms.Panel time_pnl;
    private System.Windows.Forms.PictureBox pictureBox1;
    private System.Windows.Forms.Label timeTitle_lbl;
    private System.Windows.Forms.Panel drill_pnl;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.PictureBox pictureBox2;
    private System.Windows.Forms.Label thermocoupleD_lbl;
  }
}

