==================================
Bluebit .NET Matrix Library 64 bit
==================================

This is a free version of .NET Matrix Library (NML�), which will allow matrix sizes up to
1000 rows x 1000 columns.

The Bluebit .NET Matrix Library (NML�) provides classes for object-oriented linear algebra in 
the .NET platform.

It can be used to solve systems of simultaneous linear equations, least-squares solutions of 
linear systems of equations, eigenvalues and eigenvectors problems, and singular value problems. 
Also provided are the associated matrix factorizations such as Eigen, LQ, LU, Cholesky, QR, SVD.

.NET Matrix Library (NML�) also supports sparse matrices and advanced methods for solving large 
sparse systems of linear equations.

The above functionality is present for both real and complex matrices. Two analogous sets of
classes are provided for real and complex matrices, vectors and factorizations.

While exposing an easy to use and powerful interface,  the Bluebit .NET Matrix Library does not 
sacrifice any performance. Highly optimized BLAS and the standard LAPACK routines are used within 
the library and provide fast execution and accurate calculations.

The Bluebit .NET Matrix Library has been developed as a mixed mode C++ project, combining 
together managed and unmanaged code and delivering the best of both worlds; the speed of native 
C++ code and the feature-rich and easy to use environment of the .NET Framework.
